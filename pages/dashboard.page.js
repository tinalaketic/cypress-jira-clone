export class DashboardPage {

    visit() {
        const PAGE_LOAD_TIMEOUT_MILLI = 120000
        
        cy.visit('/project/board')
        cy.get('.anticon-plus', { timeout: PAGE_LOAD_TIMEOUT_MILLI })
    }

    openCreateIssueModal() {
        cy.get('.anticon-plus')
            .click()
            .wait(1000) //HACK: modal needs some time to fully load js (event handlers, etc)
    }

    typeShortSummary(text) {
        cy.get('[formcontrolname="title"]').type(text)
    }

    typeDescription(text) {
        cy.get('[formcontrolname="description"]').type(text)
    }

    selectAssignee(assignee) {
        cy.get('issue-assignees-select input')
            .type(assignee)
            .type('{enter}')
            .click()
    }

    submitIssueForm() {
        cy.get('button').contains('Create Issue').click()
    }

    createIssue(summary, description = null, assignee = null) {
        this.openCreateIssueModal()
        this.typeShortSummary(summary)
        if (description)
            this.typeDescription(description)
        if (assignee)
            this.selectAssignee(assignee)
        this.submitIssueForm()
    }

    getIssueCard(summary) {
        return cy.get('.issue').contains(summary)
    }

    getIssueTitle() {
        return cy.get('issue-title textarea')
    }

    getIssueDescription() {
        return cy.get('issue-description')
    }

    getIssueAssignees() {
        return cy.get('issue-assignees')
    }

    saveDescription() {
        cy.get('button').contains('Save').click()
    }

    searchIssues(summary) {
        cy.get('[aria-label="search"]').type(summary)
    }

    deleteIssue() {
        cy.get('[icon="trash"] button').click()
        cy.get('button').contains('Delete').click()
    }

    addCommentToIssue(text) {
        cy.get('.editing-area').type(text)
        cy.get('span').contains('Save').click()
    }

    addCommentToIssue(text) {
        cy.get('.editing-area').type(text)
        cy.get('span').contains('Save').click()
    }

    getIssueComment(commentText) {
        return cy.contains(commentText)
    }
}
