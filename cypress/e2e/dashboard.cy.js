import { DashboardPage } from '../../pages/dashboard.page'

describe('Dashboard', () => {

    before(function() {
        this.dashboardPage = new DashboardPage()
        cy.fixture('issues/create.json').as('createData')
        cy.fixture('issues/edit.json').as('editData')
        cy.fixture('issues/search.json').as('searchData')
        cy.fixture('issues/delete.json').as('deleteData')
        cy.fixture('issues/comment.json').as('commentData')

    })

    describe('Issue', () => {

        it('should be created', function () {
            this.dashboardPage.visit()

            // Create an issue
            this.dashboardPage.createIssue(this.createData.summary, this.createData.description, this.createData.assignee)

            // Assert correct values
            this.dashboardPage.getIssueCard(this.createData.summary)
                .should('exist')
                .click()
            this.dashboardPage.getIssueTitle().should('have.value', this.createData.summary)
            this.dashboardPage.getIssueDescription()
                .contains(this.createData.description)
                .should('be.visible')
                .should('have.text', this.createData.description)
            this.dashboardPage.getIssueAssignees()
                .contains(this.createData.assignee)
                .should('be.visible')
        })

        it('should be edited', function () {
            this.dashboardPage.visit()

            // Create an issue
            this.dashboardPage.createIssue(this.editData.initialSummary, this.editData.initialDescription)

            // Open created issue
            this.dashboardPage.getIssueCard(this.editData.initialSummary)
                .should('exist')
                .click()

            // Edit the issue
            this.dashboardPage.getIssueTitle()
                .clear()
                .type(this.editData.newSummary)
            
            this.dashboardPage.getIssueDescription()
                .contains(this.editData.initialDescription)
                .click()
            cy.wait(500) //HACK: wait for the editor to load
            this.dashboardPage.getIssueDescription()
                .contains(this.editData.initialDescription)
                .type('{selectAll}{del}')
                .type(this.editData.newDescription)
            this.dashboardPage.saveDescription()

            // Assert that values are changed
            this.dashboardPage.getIssueTitle().should('have.value', this.editData.newSummary)
            this.dashboardPage.getIssueDescription()
                .contains(this.editData.newDescription)
                .should('be.visible')
                .should('have.text', this.editData.newDescription)
        })

        it('should be searchable', function () {
            this.dashboardPage.visit()

            // Create an issue
            this.dashboardPage.createIssue(this.searchData.summary)

            // Search for the issue
            this.dashboardPage.searchIssues(this.searchData.summary)

            // Assert that searched issue is visible
            this.dashboardPage.getIssueCard(this.searchData.summary)
                .should('be.visible')
        })

        it('should be deleted', function () {
            this.dashboardPage.visit()

            // Create an issue
            this.dashboardPage.createIssue(this.deleteData.summary)

            // Delete the issue
            this.dashboardPage.getIssueCard(this.deleteData.summary).click()
            this.dashboardPage.deleteIssue()

            // Assert that deleted issue is not visible
            this.dashboardPage.searchIssues(this.deleteData.summary)            
            this.dashboardPage.getIssueCard(this.deleteData.summary).should('not.exist')
        })

        it('should have comment', function () {
            this.dashboardPage.visit()

            // Create an issue
            this.dashboardPage.createIssue(this.commentData.summary)

            // Add the comment
            this.dashboardPage.getIssueCard(this.commentData.summary).click()
            this.dashboardPage.addCommentToIssue(this.commentData.comment)

            // Assert that the comment is visible
            this.dashboardPage.getIssueComment(this.commentData.comment)
                .should('exist')
                .should('be.visible')
        })
    })
})
