## Cypress E2E tests for a demo application
A demo application is a clone of Jira with basic functionalities: https://jira.trungk18.com/

### Running tests locally
``` 
npm run e2e-with-report
```
